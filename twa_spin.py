import opt_einsum as oe

import scipy.integrate
import numpy as np

import time 
import sys,os
import general_tensors as gt

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

def check_file(fname,force_overwrite) :
   if (not os.path.exists(fname)) or (os.path.exists(fname) and force_overwrite):
      if os.path.exists(fname) :
         print('WARNING FILE',fname,'EXISTS but force tag is ON, continuing')
      return True
   elif (os.path.exists(fname) and (not force_overwrite)) : 
      print('WARNING FILE',fname,'EXISTS')
      print('set force_overwrite=True')
      print('Skipping')
      return False
   else :
      print('check_file: UNKNOWN ERROR')
      sys.exit(0)

class twa_spin :
   out=[]
   ts=0
   def __init__(self,state_in,traj=0,seed0=None,verbose=False) :
      self.verbose=verbose

      self.seed0=770040000
      if not (seed0 is None) :
         self.seed0=seed0

      self.seed0+=traj
      self.traj =traj
      try :
         np.random.seed(self.seed0)
      except :
         print('invalid_seed:', self.seed0)
         self.seed0=770040000
         print('using default value instead:',self.seed0)

      self.physicalstate=state_in.copy()
      self.N=self.physicalstate.shape[0] 
         # Length of the chain is defined from
         # The length of the initial state

      if (state_in.shape[1] != 3) :
         print('input dimension does not match')
         print('shape of state_in is:',state_in.shape)
         sys.exit(0)
      self.state0=state_in.copy()

   def set_Hamil(self,h_x,h_y,h_z,J_x,J_y,J_z):
      self.h_x=h_x
      self.h_y=h_y
      self.h_z=h_z
      self.h_i=np.array([h_x,h_y,h_z])

      self.J_x=J_x
      self.J_y=J_y
      self.J_z=J_z

      self.Jij=np.array([self.J_x,self.J_y,self.J_z])
      self.epstens=gt.get_antisymtensors(3)

   def evolve(self,Tf,Tsteps,method='RK45',rtol=1e-5,atol=1e-5,verbose=None) :
      if not (verbose is None) :
         self.verbose=verbose

      self.t0=0 #it's fixed!
      self.Tf=Tf
      self.Tsteps=Tsteps
      self.ts=lisnpacenostart(self.Tf,self.Tsteps)
      self.statenow=self.physicalstate

      self.__generate_optim_path(self.statenow.copy())
      self.statenow=self.physicalstate.reshape(-1) 

      t=time.time()

      self.outputstate=scipy.integrate.solve_ivp(
            fun = self.__evolve_spin,
            t_span=[self.t0,self.ts[-1]],
            y0=self.statenow,
            t_eval=self.ts,
            method=method,
            rtol=rtol, atol=atol,
            dense_output=True
            )['y']

      if self.verbose :
         print('\n',end='')
         print('Evolution Finished: mean norm =',
             np.mean(self.outputstate.reshape(-1,3,self.ts.shape[0])[:,0,-1]**2.
            + self.outputstate.reshape(-1,3,self.ts.shape[0])[:,1,-1]**2. 
            + self.outputstate.reshape(-1,3,self.ts.shape[0])[:,2,-1]**2.)) # should be 1.

      # End
      elapsed = time.time() - t
      print('Evolution Finished:', elapsed, ' seconds (', elapsed/60., ' minutes)')

   def __generate_optim_path(self,y) :
      self.optim_single=oe.contract_path('bi,ig,abg -> ia',
            np.array(self.h_i),y,self.epstens,optimize='optimal')[0]
      self.optim_double_1=oe.contract_path('bpi,pb,ig,abg-> ia',
            self.Jij,y,y,self.epstens,optimize='optimal')[0]
      self.optim_double_2=oe.contract_path('biq,qb,ig,abg -> ia',
            self.Jij,y,y,self.epstens,optimize='optimal')[0]

   def __evolve_spin(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=True)
      ynow=y.reshape(-1,3)
      ydot = \
            oe.contract('bi,ig,abg -> ia',self.h_i,ynow,self.epstens,optimize=self.optim_single) \
            + \
            oe.contract('bpi,pb,ig,abg-> ia',self.Jij,ynow,ynow,self.epstens,optimize=self.optim_double_1) \
            + \
            oe.contract('biq,qb,ig,abg -> ia',self.Jij,ynow,ynow,self.epstens,optimize=self.optim_double_2)

      ydot=ydot.reshape(-1)
      return ydot

   # output 
   def output_obs(self, fname, Op='x', force_overwrite=False):
      obsind=0
      if Op.lower() == 'x' :
         obsind=0
      elif Op.lower() == 'y' :
         obsind=1
      elif Op.lower() == 'z' :
         obsind=2
      else :
         print('twa_spin.py: error: Op:',Op,'does not exist')
         return 0 

      if not(check_file(fname,force_overwrite)): 
         return 0 

      obs=self.outputstate.reshape(-1,3,self.ts.shape[0])[:,obsind,:]
      print(obs[:,0])
      np.savetxt(fname,obs.transpose(),delimiter=',',fmt='%2.15f')

   def get_outputstate(self):
      if not(check_file(fname,force_overwrite)): 
         return 0 
      return self.outputstate

   def get_timevec(self,fname,force_overwrite=False):
      if not(check_file(fname,force_overwrite)): 
         return 0 
      np.savetxt(fname,self.ts,delimiter=',',fmt='%2.15f')

   # Debug Functions: It doesn't mean that they are obsolete.
   def get_Orthomat_mats(self) : 
      return self.Orthomat_mats

   def get_Orthomat_coeffs(self) :
      return self.Orthomat_coeffs

   def check_Orthomat_decomposition(self) :
      return om.check_decomposition(self.physicalmats,self.Orthomat_mats,self.Orthomat_coeffs)

   def get_initial(self) :
      return self.state0
   ##########################################################
