import sys,os
sys.path.append('../')

import twa_spin as ts
import numpy as np

from scipy import integrate #import DOP853
from datetime import datetime 

print('Program started at:',datetime.now())

verbose=False
overwrite=False

verbose=True
overwrite=True

dphi=0.001

J=1.0
Wq=0.0
sval=0

def check_pwr2(d) :
   d_init=d
   if d==1 :
      return 1.
   elif d==0 :
      return 0.
   else :
      while d>1 :
         if d%2 == 1 :
            return 0.
         else :
            d=d//2
      if d==1 :
         return float(d_init)
      else :
         return 0.
   return -1

L=64
seedbase=1666514987
traj=0
print('L={0}, J={1:.2f}, Wq={2:.2f}'.format(L,J,Wq))

tf=0.01
dt=0.005


np.random.seed(seedbase+traj)

state_in=[]
dtheta_z=0.01
theta_initial=[]
phi_initial=[]
for ind in range(L) :
   theta=np.pi/2.0+np.random.uniform(-1.0,1.0)*dtheta_z
   phi=np.random.uniform(0,1.0)*2*np.pi

   theta_initial.append(theta)
   phi_initial.append(phi)
   currstate=[np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]

   state_in.append(currstate.copy())

state_in=np.array(state_in)

h_x=np.zeros(L,dtype=np.complex128)
h_y=np.zeros(L,dtype=np.complex128)
h_z=Wq*np.array(np.random.uniform(-1,1,L),dtype=np.complex128)/2.

J_x=np.zeros((L,L),dtype=np.complex128)
for indi in range(L) :
   for indj in range(L) :
      fac=check_pwr2(min(abs(indj-indi),L-abs(indj-indi))) 
      if fac > 0 :
         J_x[indi,indj] = fac**sval

J_x *= -J
J_y = J_x.copy()

J_z = np.zeros((L,L),dtype=np.complex128)

testvar=ts.twa_spin(state_in,traj=traj,verbose=verbose)
testvar.set_Hamil(h_x,h_y,h_z,J_x,J_y,J_z)
testvar.evolve(tf,int(tf/dt),method="LSODA",rtol=1e-14,atol=1e-14) 
outdir='./testdata/{0:.5f}/{1:.5f}/init/s_{2:.5f}/'.format(tf,dt,sval)
if not os.path.exists(outdir) :
   os.makedirs(outdir)
fname=outdir+'traj_{0:d}_Sz.out'.format(traj)
testvar.output_obs(fname, Op='z', force_overwrite=overwrite)


state_in=[]
dtheta_z=0.01

for ind in range(L) :
   theta=theta_initial[ind] 
   phi=phi_initial[ind]+dphi*(ind==0)
   currstate=[np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]
   state_in.append(currstate.copy())

state_in=np.array(state_in)

testvar=ts.twa_spin(state_in,traj=traj,verbose=verbose)
testvar.set_Hamil(h_x,h_y,h_z,J_x,J_y,J_z)
testvar.evolve(tf,int(tf/dt),verbose=verbose,method="LSODA",rtol=1e-14,atol=1e-14)
outdir='./testdata/{0:.5f}/{1:.5f}/s_{2:.5f}/dphi_{3:.5f}/'.format(tf,dt,sval,dphi)
if not os.path.exists(outdir) :
   os.makedirs(outdir)
fname=outdir+'traj_{0:d}_Sz.out'.format(traj)
testvar.output_obs(fname, Op='z', force_overwrite=overwrite)

outdir='./testdata/{0:.5f}/{1:.5f}/'.format(tf,dt)
testvar.get_timevec(outdir+'times.out',overwrite)

print(state_in.shape)
print(state_in[:,2])
