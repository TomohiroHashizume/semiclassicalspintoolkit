import numpy as np
import sys,os
import matplotlib.pyplot as plt

def generate_fname(traj,tf,dt,sval,dphi,init=False):
   init_tag=''
   if init:
      return './testdata/{0:.5f}/{1:.5f}/init/s_{2:.5f}/traj_{3:d}_Sz.out'.format(tf,dt,sval,traj)
   return './testdata/{0:.5f}/{1:.5f}/{2:.5f}/dphi_{3:.5f}/traj_{4:d}_Sz.out'.format(tf,dt,sval,dphi,traj)

traj=0
sval=0
dphi=0.001

tf=0.01
dt=0.005

init=True
fname=generate_fname(traj,tf,dt,sval,dphi,init=init)
print(fname)

if os.path.exists(fname):
   M=np.loadtxt(fname,delimiter=',')
   plt.plot(M[0])
   plt.plot(M[-1])
   plt.show()
