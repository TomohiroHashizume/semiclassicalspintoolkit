# Semiclassical Spin Toolkit

The software implements semiclassical simulation of spin systems based on the supplementary material in Bentsen et al. (2019).

This software is not tested yet (there isn't any publication based on this software). 

## Cite 
Please cite the following:

The software is based on:
* Gregory Bentsen, Tomohiro Hashizume, Anton S. Buyskikh, Emily J. Davis, Andrew J. Daley, Steven S. Gubser, Monika Schleier-Smith. (2019).
[Phys. Rev. Lett. 123, 130601](https://doi.org/10.1103/PhysRevLett.123.130601)

Cite this software as:
* Tomohiro Hashizume. (2022). Semiclassical Spin Toolkit 0.1. [https://doi.org/10.5281/zenodo.7245801](https://doi.org/10.5281/zenodo.7245801).

Please include the date visited. 

## TODO

Complete this read me...

## Usage

Please see the example codes in `test`

## Contact

* Tomohiro Hashizume (original developer) tomohiro.hashizume.overseas@gmail.com

## License

MIT License

Copyright (c) [2022] [Tomohiro Hashizume]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Ongoing...
