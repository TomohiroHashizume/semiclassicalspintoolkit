import sys,os

import scipy.integrate
import numpy as np

import time 

# VERSION USED FOR 

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

class dtwa_spin :
   def __init__(self,state_in,seed=1,verbose=False) :
      self.verbose=verbose
      self.seed=seed
      np.random.seed(self.seed)

      self.physicalstate=state_in
      self.N=self.physicalstate.shape[0] 
         # Length of the chain is defined from
         # The length of the initial state

      if (state_in.shape[1] != 3) :
         print('input dimension does not match')
         sys.exit(0)
      self.state0=state_in.copy()

   def set_Hamil(self,h_x,h_y,h_z,J_x,J_y,J_z):
      self.h_x=h_x
      self.h_y=h_y
      self.h_z=h_z
      self.h_i=[h_x,h_y,h_z]

      self.J_x=J_x
      self.J_y=J_y
      self.J_z=J_z

      self.Jij=[self.J_x,self.J_y,self.J_z]

   def evolve(self,Tf,Tsteps,outdir='./',Op='Sz',method='RK45',rtol=1e-5,atol=1e-5) :
      self.outdir=outdir
      self.Op=Op
      self.t0=0 #it's fixed!
      self.Tf=Tf
      self.Tsteps=Tsteps
      self.ts=lisnpacenostart(self.Tf,self.Tsteps)
      self.statenow=self.physicalstate
      self.fname=self.__generate_fname()

      self.statenow=self.physicalstate.reshape(-1)

      t=time.time()

      self.out=scipy.integrate.solve_ivp(
            fun = self.__evolve_spin,
            t_span=[self.t0,self.ts[-1]],
            y0=self.statenow,
            t_eval=self.ts,
            method=method,
            rtol=rtol, atol=atol,
            dense_output=True
            )['y']
      obs=self.out.reshape(-1,3,self.ts.shape[0])[:,2,:]
      obs=obs[:(self.N//2),:] - obs[(self.N//2):,:]
      self.fout=open(self.fname+'.out','w')
      self.fout.close()
      for i  in range(obs.shape[1]) :
         obstmp=obs[:,i]
         self.fout=open(self.fname+'.out','a')
         np.savetxt(self.fout,[obstmp],delimiter=',',fmt='%2.15f')
         self.fout.close()

      elapsed = time.time() - t
      print('One timestep finished:', elapsed, ' seconds (', elapsed/60., ' minutes)')
      #self.fout.close()

   def __generate_fname(self) :
      if not os.path.exists(self.outdir) :
         os.makedirs(self.outdir)
      return self.outdir+"/traj_{0}".format(self.seed)

   def __evolve_spin(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=True)
      ynow=y.reshape(-1,3)

      ynow1=ynow[self.N//2:,:]
      ynow=ynow[:self.N//2,:] 

      ydot=np.empty(ynow.shape)
      ydot1=np.empty(ynow1.shape)

      ydot[:,0] = ynow[:,2]*(self.J_x @ ynow[:,1])
      ydot[:,1] = -ynow[:,2]*(self.J_x @ ynow[:,0])
      ydot[:,2] = ynow[:,1]*(self.J_x @ ynow[:,0]) - ynow[:,0]*(self.J_x @ ynow[:,1])

      ydot1[:,0] = ynow1[:,2]*(self.J_x @ ynow1[:,1])
      ydot1[:,1] = -ynow1[:,2]*(self.J_x @ ynow1[:,0])
      ydot1[:,2] = ynow1[:,1]*(self.J_x @ ynow1[:,0]) - ynow1[:,0]*(self.J_x @ ynow1[:,1])

      ydot=np.vstack((ydot,ydot1))
      ydot=ydot.reshape(-1)

      return ydot

   # Debug Functions: It doesn't mean that they are obsolete.
   def get_Orthomat_mats(self) : 
      return self.Orthomat_mats

   def get_Orthomat_coeffs(self) :
      return self.Orthomat_coeffs

   def check_Orthomat_decomposition(self) :
      return om.check_decomposition(self.physicalmats,self.Orthomat_mats,self.Orthomat_coeffs)

   def get_initial(self) :
      return self.state0
   ##########################################################
