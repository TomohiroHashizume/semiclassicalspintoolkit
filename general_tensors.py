import itertools as it
import numpy as np

def is_even(p):
   if len(p) == 1:
      return True

   trans = 0
   for i in range(0,len(p)):
      j = i + 1

      for j in range(j, len(p)):
         if p[i] > p[j]: 
            trans = trans + 1

   if ((trans % 2) == 0) :
      return 1 
   else :
      return -1 

def get_antisymtensors(numind) :
   out = np.zeros([numind]*numind,dtype=int)
   for perm in it.permutations(range(3)) :
      out[perm]=is_even(perm)
   return out
